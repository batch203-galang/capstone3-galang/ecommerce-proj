import {useEffect, useState} from "react";

import {BrowserRouter as Router, Routes, Route} from "react-router-dom";

import {Container} from "react-bootstrap";

import AdminDashboard from "./pages/AdminDashboard";
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Orders from "./pages/Orders";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";


import './App.css';

import {UserProvider} from "./UserContext";


function App() {

  // Global state hooks for the user information for validating if the user is logged in.
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing local storage
  const unsetUser = () => {
    // Allow us to clear the information in the localStorage
    localStorage.clear();
    }

    useEffect(() =>{
      console.log(user);
      console.log(localStorage);
    }, [user])

    // To update the User state upon a page load is initiated and a user already exists.
    useEffect(()=>{

        fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
          }
        })
          .then(res => res.json())
          .then(data => {
            console.log(data);

            // Set the user states value if the token already exists
           if(data._id !== undefined){
               setUser({
                id: data._id,
                isAdmin: data.isAdmin
              });
            }
            else {
              setUser({
                id: null,
                isAdmin: null
              });
            }  
          })
        }, []) 

  return (
    
      <UserProvider value={{user, setUser, unsetUser}}>
       
      <Router>
        <AppNavbar />
        <Container >
          {/*Routes holds all our Route components*/}
          <Routes>
          
            <Route exact path="/" element={<Home/>} />
            <Route exact path="/admin" element={<AdminDashboard/>} />
            <Route exact path="/products" element={<Products/>} />
            <Route exact path="/orders" element={<Orders/>} />
            <Route exact path="/products/:productId" element={<ProductView/>} />
            <Route exact path="/login" element={<Login/>} />
            <Route exact path="/register" element={<Register/>} />
            <Route exact path="/logout" element={<Logout/>} />
            <Route path="*" element={<Error/>}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
