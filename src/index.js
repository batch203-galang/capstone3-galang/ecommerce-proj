import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Import a Bootstrap 5 CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>

  // React.StrictMode is a built-in react component which is used to higlight potential error
);
