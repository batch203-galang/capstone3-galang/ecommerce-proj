import {useState, useEffect} from "react";

import { Container, Table} from "react-bootstrap";

// import UserContext from "../UserContext";

export default function Orders(){

	const [allUsers, setAllUsers] = useState([])

	const fetchUserOrders = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.orders.map(order => {
				return(
					<tr key={order._id}>
						<td>{order.products[0].productName}</td>
						<td>{order.products[0].quantity}</td>
						<td>{order.totalAmount}</td>
					</tr>
					)
				}));
			});
		}

	useEffect(() => {
		fetchUserOrders();
	}, []);


	return (
		<Container className="mt-5">
		<h2 >Order Lists</h2>
           <Table striped bordered hover>
			     <thead>
			       <tr>
			         <th>Total Amount</th>
			         <th>Product Name</th>
			         <th>Quantity</th>
			       </tr>
			     </thead>
			     <tbody>
			       {allUsers}
			     </tbody>
			   </Table>
        </Container>
	)
}
