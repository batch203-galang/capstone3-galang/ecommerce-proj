import {useState, useEffect} from "react";

import {useNavigate} from "react-router-dom";
import {Card, Container, Form, Button} from "react-bootstrap";
import Swal from "sweetalert2";


export default function Register(){

	// const {user} = useContext(UserContext);
	const navigate = useNavigate();
	
	// Create state hooks to store the values of the input fields
	const [fName, setFName] = useState('');
	const [lName, setLName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);
		
	useEffect(()=>{

		// Enable the button if:
			// All the fields are populated
			// Both paswords match
		if((fName !== '' && lName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '')&&(password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [fName, lName, email, mobileNo, password1, password2])

	// Function to simulate user registration
	function registerUser(e) {
		e.preventDefault(); // prevents page loading/redirection via form submission

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify ({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Duplicate email found!",
					icon: "error",
					text: "Please provide another email to complete your registration."
				})
			}
			else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: fName,
						lastName: lName,
						email: email,
						mobileNumber: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data) {
						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "Kindly log-in first."
						})

							// Clear input fields
							setFName('');
							setLName('');
							setEmail('');
							setMobileNo('');
							setPassword1('');
							setPassword2('');

							// Allow us to redirect the user to the login page after account registration
							navigate("/login");
					}
					else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})
	}

	return(
		<>
		<Container className="font-link p-4 w-50" >
		<Card>
		<Card.Body>
			<h1 className="my-4 text-center">Register</h1>
			<Form onSubmit={e => registerUser(e)}>

			 <Form.Group className="mb-3" controlId="firstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Enter first name" 
		        	value={fName}
		        	onChange={e => setFName(e.target.value)}
		        	required
		        />
		     </Form.Group>

			  <Form.Group className="mb-3" controlId="lastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Enter last name"
		        	value={lName} 
		        	onChange={e => setLName(e.target.value)}
		        	required
		        />
		     </Form.Group>



		      <Form.Group className="mb-3" controlId="emailAddress">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        	type="email" 
		        	placeholder="Enter email"
		        	value={email}
		        	onChange={e => setEmail(e.target.value)}
		        	required
		         />
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="mobileNo">
		        <Form.Label>Mobile number</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="09xx-xxx-xxxx"
		        	value={mobileNo}
		        	onChange={e => setMobileNo(e.target.value)} 
		        	required
		        />
		     </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	// placeholder="Enter password"
		        	value={password1}
		        	onChange={e => setPassword1(e.target.value)} 
		        	required 
		        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Confirm Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	// placeholder="Confirm password"
		        	value={password2}
		        	onChange={e => setPassword2(e.target.value)}
		        	required 
		        />
		      </Form.Group>
		  	  {/* Conditional Rendering - submit button will be active based on the isActive state*/}
		  	  {
		  	  	isActive 
		  	  	?
			  	  	<Container className="p-2 text-center">
			  	  	<Button variant="dark" type="submit" id="submitBtn">
			        Submit
			      	</Button>
			      	</Container>
			    :
			    	<Container className="p-2 text-center">
			    	<Button variant="secondary" type="submit" id="submitBtn" disabled>
			        Submit
			      	</Button>
			      	</Container>
		  	  }
		  	  <p className="text-center">Already have an account? <a href="/login">Login here!</a></p>
		      
		    </Form>
		    </Card.Body>
		   </Card>
		   </Container>
		</>
	)
}