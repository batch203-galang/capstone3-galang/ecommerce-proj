import Banner from "../components/Banner";

export default function Home() {

	const data = {
		title: "Ibywind Philippines",
		description: "Ibywind Philippines' tempered glass is sleek, elegant and sophisticated to fit your everyday lifestyle. We are dedicated to provide the perfect balance of high grade protection and innovation styles that amplify the beauty of your smartphone.",
		destination: "/products",
		label: "Shop Now"
	}

	return (
		<>
			<Banner bannerProp={data}/>
		</>
	)
}